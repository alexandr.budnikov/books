<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Loader,
    Bitrix\Iblock;

class CBooks extends CBitrixComponent
{
	public function executeComponent()
    {
        if(!Loader::includeModule("iblock"))
        {
            $this->abortResultCache();
            ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
            return;
        }

        if($this->startResultCache())
        {
            $this->arResult = $this->findBooks();
            $this->includeComponentTemplate();
        }
        return $this->arResult["Y"];
    }

    public function findBooks()
    {
        $arFilter = array(
            'ACTIVE' => 'Y',
            'IBLOCK_ID' => $this->arParams['IBLOCK_ID']
        );
        $arSelect = array('ID', 'IBLOCK_ID', 'NAME', 'PREVIEW_TEXT', 'PROPERTY_*');

    	$rsElement = CIBlockElement::GetList($arSort, $arFilter, false, $arNavParams, $arSelect);
        
        while($obElement = $rsElement->GetNextElement())
        {
            $arItem = $obElement->GetFields();
            $arProps = $obElement->GetProperties();
            $arItem['PROPERTIES'] = $arProps;
            
            $arResult["ITEMS"][] = $arItem;
        }
        return $arResult;
    }

    public function deleteBook($id)
    {
        //CIBlockElement::Delete($id);
        return true;
    }
}?>