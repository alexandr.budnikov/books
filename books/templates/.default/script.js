$( document ).ready(function() {
    $('table.books-table td.delete-book a').on('click', function(){
    	var this_link = $(this);
    	var book_id = $(this_link).parents('td').attr('data-id');
    	$.ajax({
    		type: 'POST',
    		url: '/local/components/baris/books/ajax.php',
    		data: 'book_id='+book_id,
    		success: function(){
    			$(this_link).parents('tr').remove();
    		}
    	});
    	return false;
    });
});