<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();?>
<table class="books-table">
	<tr>
		<th class="title">Название</th>
		<th class="descr">Описание</th>
		<th class="author">Автор</th>
		<th class="year">Год выпуска</th>
		<th class="del">X</th>
	</tr>
	<? foreach ($arResult['ITEMS'] as $arItem): ?>
		<tr>
			<td><?=$arItem['NAME'];?></td>
			<td><?=$arItem['PREVIEW_TEXT'];?></td>
			<td><?=$arItem['PROPERTIES']['AUTHOR']['VALUE'];?></td>
			<td><?=$arItem['PROPERTIES']['YEAR']['VALUE'];?></td>
			<td class="delete-book" data-id="<?=$arItem['ID'];?>"><a href="javascript:void(0);">X</a></td>
		</tr>
	<? endforeach; ?>
</table>